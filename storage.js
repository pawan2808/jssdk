var storage = function(){};

storage.prototype.setLocalData = function(name,data){
    localStorage.setItem(name, data);
  };

  storage.prototype.getLocalData = function(name) {
    return localStorage.getItem(name);
};

storage.prototype.setSessionData = function(name,data){
    sessionStorage.setItem(name, data);
  };

  storage.prototype.getSessionData = function(name) {
  return sessionStorage.getItem(name);
};

var storageutil = new storage();

export {storageutil};