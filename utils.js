import {builder} from './builder/jsonBuilder.js';
import {sessionBuilder} from './builder/session.js';
import {geoBuilder} from './builder/geo.js';
import {metaBuilder} from './builder/meta.js';
import {viewportBuilder} from './builder/viewport.js';
import {eventBuilder} from './builder/eventdata.js';
import {eventinfoBuilder} from './builder/eventinfo.js';
import {positionBuilder} from './builder/position.js';
import {sdkVersion} from './config.js';
    var utilsFunc = function () {};

    utilsFunc.prototype.creatSessionObject = function(eventName) {
        const builder = new sessionBuilder(); 
        const geoObject = this.creatGeoObject(); 
        const metaObject = this.creatMetaObject();
        const viewportObject = this.creatViewPortObject(); 
        const eventData = this.creatEventsDataObject(eventName); 
        return builder
            .startTime(Date.now())
            .endTime('')
            .lastServerSyncTime('')
            .sdkVersion(sdkVersion)
            .geo(geoObject)
            .meta(metaObject)
            .viewPort(viewportObject)
            .eventsData(eventData)
            .build();  
    };

    utilsFunc.prototype.creatGeoObject = function() {
        const builder = new geoBuilder(); 
        return builder
                .conc('AS')
                .couc('IN')
                .reg('MH')
                .city('Pune')
                .zip(411007)
                .build();
    };

    utilsFunc.prototype.creatMetaObject = function() {
        const builder = new metaBuilder(); 
        var os = this.findOS();
        const browserDetails = this.browserDetails();
        return builder
                .plf(11)
                .domain(window.location.hostname)
                .dcomp(0)
                .acomp(0)
                .osv(navigator.appVersion)
                .dmft('')
                .dm('')
                .hostOS(os)
                .browser(browserDetails.browserName)
                .version(browserDetails.majorVersion)
                .build();
    };

    utilsFunc.prototype.creatViewPortObject = function() {
        const builder = new viewportBuilder(); 
        return builder
                .timeStamp(Date.now())
                .width(document.documentElement.clientWidth)
                .height(document.documentElement.clientHeight)
                .build();
    };

    utilsFunc.prototype.creatEventsDataObject = function(eventName) {
        const builder = new eventBuilder(); 
        const navigationPath = [window.location.href];
        const eventInfoDetails = this.getEventInfo(eventName);
        return builder
                .eventsInfo(eventInfoDetails)
                .navigationPath(navigationPath)
                .stayTimeBeforeNav([])
                .devCodifiedEventsInfo([])
                .sentToServer(false)
                .build();
    };

    utilsFunc.prototype.getEventInfo = function(eventName) {
        const builder = new eventinfoBuilder(); 
        const postionObj = this.getPostionObj();
        var info = [];
        var obj = builder
                .sentToServer(false)
                .objectName(eventName)
                .name(eventName)
                .urlPath(window.location.href)
                .tstmp(Date.now())
                .position(postionObj)
                .metaInfo({})
                .build();  
        info.push(obj);
        return info;        
    };



    utilsFunc.prototype.getPostionObj = function() {
        const builder = new positionBuilder(); 
        return builder
                    .x('')
                    .y('')
                    .width('')
                    .height('')
                    .build();       
    };

    utilsFunc.prototype.findOS = function() {
        var curOS="";
        if (navigator.appVersion.indexOf("Win")!=-1) curOS="Windows";
        if (navigator.appVersion.indexOf("Mac")!=-1) curOS="MacOS";
        if (navigator.appVersion.indexOf("X11")!=-1) curOS="UNIX";
        if (navigator.appVersion.indexOf("Linux")!=-1) curOS="Linux";
        return curOS;
      }

    utilsFunc.prototype.browserDetails = function() {
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browserName  = navigator.appName;
        var fullVersion  = ''+parseFloat(navigator.appVersion); 
        var majorVersion = parseInt(navigator.appVersion,10);
        var nameOffset,verOffset,ix;

        // In Opera, the true version is after "Opera" or after "Version"
        if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
        browserName = "Opera";
        fullVersion = nAgt.substring(verOffset+6);
        if ((verOffset=nAgt.indexOf("Version"))!=-1) 
        fullVersion = nAgt.substring(verOffset+8);
        }
        // In MSIE, the true version is after "MSIE" in userAgent
        else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
        browserName = "Microsoft Internet Explorer";
        fullVersion = nAgt.substring(verOffset+5);
        }
        // In Chrome, the true version is after "Chrome" 
        else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
        browserName = "Chrome";
        fullVersion = nAgt.substring(verOffset+7);
        }
        // In Safari, the true version is after "Safari" or after "Version" 
        else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
        browserName = "Safari";
        fullVersion = nAgt.substring(verOffset+7);
        if ((verOffset=nAgt.indexOf("Version"))!=-1) 
        fullVersion = nAgt.substring(verOffset+8);
        }
        // In Firefox, the true version is after "Firefox" 
        else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
        browserName = "Firefox";
        fullVersion = nAgt.substring(verOffset+8);
        }
        // In most other browsers, "name/version" is at the end of userAgent 
        else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
                (verOffset=nAgt.lastIndexOf('/')) ) 
        {
        browserName = nAgt.substring(nameOffset,verOffset);
        fullVersion = nAgt.substring(verOffset+1);
        if (browserName.toLowerCase()==browserName.toUpperCase()) {
        browserName = navigator.appName;
        }
        }
        // trim the fullVersion string at semicolon/space if present
        if ((ix=fullVersion.indexOf(";"))!=-1)
        fullVersion=fullVersion.substring(0,ix);
        if ((ix=fullVersion.indexOf(" "))!=-1)
        fullVersion=fullVersion.substring(0,ix);

        majorVersion = parseInt(''+fullVersion,10);
        if (isNaN(majorVersion)) {
        fullVersion  = ''+parseFloat(navigator.appVersion); 
        majorVersion = parseInt(navigator.appVersion,10);
        }

        return {browserName,majorVersion};
    }  

    var utility = new utilsFunc();
    export {utility};