var object = function(plf,domain,dcomp,acomp,osv,dmft,dm,hostOS,browser,version){
    this.plf = plf;
    this.domain = domain;
    this.dcomp = dcomp;
    this.acomp = acomp;
    this.osv = osv;
    this.dmft = dmft;
    this.dm = dm;
    this.hostOS = hostOS;
    this.browser = browser;
    this.version = version;
  };
  
  var metaBuilder = function(){};
    
  metaBuilder.prototype.plf = function(plf){
      this._plf = plf;
      return this;
  };
    
  metaBuilder.prototype.domain = function(domain){
      this._domain = domain;
      return this;
  };
    
  metaBuilder.prototype.dcomp = function(dcomp){
      this._dcomp = dcomp;
      return this;
  };
    
  metaBuilder.prototype.acomp = function(acomp){
      this._acomp = acomp;
      return this;
  };
    
  metaBuilder.prototype.osv = function(osv){
      this._osv = osv;
      return this;
  };
    
  metaBuilder.prototype.dmft = function(dmft){
      this._dmft = dmft;
      return this;
  };
    
    metaBuilder.prototype.dm = function(dm){
      this._dm = dm;
      return this;
    };
    
    metaBuilder.prototype.hostOS = function(hostOS){
      this._hostOS = hostOS;
      return this;
    };
    
    metaBuilder.prototype.browser = function(browser){
        this._browser = browser;
        return this;
    };
    
    metaBuilder.prototype.version = function(version){
      this._version = version;
      return this;
    };
  
    metaBuilder.prototype.build = function() {
    var obj = new object(this._plf,
                        this._domain,
                        this._dcomp,
                        this._acomp,
                        this._osv,
                        this._dmft,
                        this._dm,
                        this._hostOS,
                        this._browser,
                        this._version);
    return obj;
  };
  
  export {metaBuilder};