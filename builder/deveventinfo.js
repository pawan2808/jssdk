var object = function(sentToServer,objectName,name,urlPath,tstmp,duration,metaInfo){
    this.sentToServer = sentToServer;
    this.objectName = objectName;
    this.name = name;
    this.urlPath = urlPath;
    this.tstmp = tstmp;
    this.duration = duration;
    this.metaInfo = metaInfo;
  };
  
  var deveventinfoBuilder = function(){};
    
    deveventinfoBuilder.prototype.sentToServer = function(sentToServer){
      this._sentToServer = sentToServer;
      return this;
    };
    
    deveventinfoBuilder.prototype.objectName = function(objectName){
        this._objectName = objectName;
        return this;
    };

     deveventinfoBuilder.prototype.name = function(name){
        this._name = name;
        return this;
     };
      
     deveventinfoBuilder.prototype.urlPath = function(urlPath){
         this._urlPath = urlPath;
         return this;
     };
    
     deveventinfoBuilder.prototype.tstmp = function(tstmp){
       this._tstmp = tstmp;
       return this;
     };
    
     deveventinfoBuilder.prototype.duration = function(duration){
       this._duration = duration;
       return this;
     };
    
    deveventinfoBuilder.prototype.metaInfo = function(metaInfo){
      this._metaInfo = metaInfo;
      return this;
    };
  
    deveventinfoBuilder.prototype.build = function() {
    var obj = new object(this._sentToServer,
                        this._objectName,
                        this._name,
                        this._urlPath,
                        this._tstmp,
                        this._duration,
                        this._metaInfo);
    return obj;
  };
  
  export {deveventinfoBuilder};