var object = function(conc,couc,reg,city,zip){
    this.conc = conc;
    this.couc = couc;
    this.reg = reg;
    this.city = city;
    this.zip = zip;
  };
  
  var geoBuilder = function(){};
    
    geoBuilder.prototype.conc = function(conc){
        this._conc = conc;
        return this;
    };
    
    geoBuilder.prototype.couc = function(couc){
      this._couc = couc;
      return this;
    };
    
    geoBuilder.prototype.reg = function(reg){
      this._reg = reg;
      return this;
    };
    
    geoBuilder.prototype.city = function(city){
        this._city = city;
        return this;
    };
    
    geoBuilder.prototype.zip = function(zip){
      this._zip = zip;
      return this;
    };
  
    geoBuilder.prototype.build = function() {
    var obj = new object(this._conc,
                        this._couc,
                        this._reg,
                        this._city,
                        this._zip);
    return obj;
  };
  
  export {geoBuilder};