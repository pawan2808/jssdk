import {geoBuilder} from './geo.js';
import {metaBuilder} from './meta.js';
import {viewportBuilder} from './viewport.js';
import {eventBuilder} from './eventdata.js';
var object = function(startTime,endTime,lastServerSyncTime,sdkVersion,geo,meta,viewPort,eventsData){
    this.startTime = startTime;
    this.endTime = endTime;
    this.lastServerSyncTime = lastServerSyncTime;
    this.sdkVersion = sdkVersion;
    this.geo = geo;
    this.meta = meta;
    this.viewPort = viewPort;
    this.eventsData = eventsData;
  };
  
  var sessionBuilder = function(){};
  sessionBuilder.prototype.startTime = function(startTime){
    this._startTime = startTime;
    return this;
    };
    
    sessionBuilder.prototype.endTime = function(endTime){
      this._endTime = endTime;
      return this;
    };
    
    sessionBuilder.prototype.lastServerSyncTime = function(lastServerSyncTime){
      this._lastServerSyncTime = lastServerSyncTime;
      return this;
    };
    
    sessionBuilder.prototype.sdkVersion = function(sdkVersion){
        this._sdkVersion = sdkVersion;
        return this;
    };
    
    sessionBuilder.prototype.geo = function(geo){
      const builder = new geoBuilder();
      this._geo = builder
            .conc(geo.conc)
            .couc(geo.couc)
            .reg(geo.reg)
            .city(geo.city)
            .zip(geo.zip)
            .build();
      return this;
    };

    sessionBuilder.prototype.meta = function(meta){
        const builder = new metaBuilder();
        this._meta = builder
                .plf(meta.plf)
                .domain(meta.domain)
                .dcomp(meta.dcomp)
                .acomp(meta.acomp)
                .osv(meta.osv)
                .dmft(meta.dmft)
                .dm(meta.dm)
                .hostOS(meta.hostOS)
                .browser(meta.browser)
                .version(meta.version)
                .build();
      return this;
    };
    
    sessionBuilder.prototype.viewPort = function(viewPort){
        const builder = new viewportBuilder();
        this._viewPort = builder
                .timeStamp(viewPort.timeStamp)
                .width(viewPort.width)
                .height(viewPort.height)
                .build();
      return this;
    };
    
    sessionBuilder.prototype.eventsData = function(eventsData){
      const builder = new eventBuilder();
        this._eventsData = builder
                .eventsInfo(eventsData.eventsInfo)
                .navigationPath(eventsData.navigationPath)
                .stayTimeBeforeNav(eventsData.stayTimeBeforeNav)
                .devCodifiedEventsInfo(eventsData.devCodifiedEventsInfo)
                .sentToServer(eventsData.sentToServer)
                .build();
      return this;
    };
  
    sessionBuilder.prototype.build = function() {
    var obj = new object(this._startTime,
                        this._endTime,
                        this._lastServerSyncTime,
                        this._sdkVersion,
                        this._geo,
                        this._meta,
                        this._viewPort,
                        this._eventsData);
    return obj;
  };
  
//   var sessionbuilder = new sessionBuilder();
  export {sessionBuilder};