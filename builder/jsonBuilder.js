import {sessionBuilder} from './session.js';
var object = function(date,domain,tag,sessions,retentionData){
  this.date = date;
  this.domain = domain;
  this.tag = tag;
  this.sessions = sessions;
  this.retentionData = retentionData;
};

var jsonBuilder = function(){};
jsonBuilder.prototype.date = function(date){
    this._date = date;
    return this;
  };
  
  jsonBuilder.prototype.domain = function(domain){
    this._domain = domain;
    return this;
  };
  
  jsonBuilder.prototype.tag = function(tag){
    this._tag = tag;
    return this;
  };
  
  jsonBuilder.prototype.sessions = function(session){
    const builder = new sessionBuilder();            
    this._sessions = session.map((session) => {
      return builder
            .startTime(session.startTime)
            .endTime(session.endTime)
            .lastServerSyncTime(session.lastServerSyncTime)
            .sdkVersion(session.sdkVersion)
            .geo(session.geo)
            .meta(session.meta)
            .viewPort(session.viewPort)
            .eventsData(session.eventsData)
            .build();      
      });
    return this;
  };
  
  jsonBuilder.prototype.retentionData = function(retentionData){
    this._retentionData = retentionData;
    return this;
  };

jsonBuilder.prototype.build = function() {
  var obj = new object(this._date,this._domain,this._tag,this._sessions,this._retentionData);
  return obj;
};

var builder = new jsonBuilder();
export {builder};