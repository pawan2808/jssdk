import {positionBuilder} from './position.js';
var object = function(sentToServer,objectName,name,urlPath,tstmp,position,metaInfo){
    this.sentToServer = sentToServer;
    this.objectName = objectName;
    this.name = name;
    this.urlPath = urlPath;
    this.tstmp = tstmp;
    this.position = position;
    this.metaInfo = metaInfo;
  };
  
  var eventinfoBuilder = function(){};
    
    eventinfoBuilder.prototype.sentToServer = function(sentToServer){
      this._sentToServer = sentToServer;
      return this;
    };
    
    eventinfoBuilder.prototype.objectName = function(objectName){
        this._objectName = objectName;
        return this;
    };

     eventinfoBuilder.prototype.name = function(name){
        this._name = name;
        return this;
     };
      
     eventinfoBuilder.prototype.urlPath = function(urlPath){
         this._urlPath = urlPath;
         return this;
     };
    
    eventinfoBuilder.prototype.tstmp = function(tstmp){
      this._tstmp = tstmp;
      return this;
    };
    
    eventinfoBuilder.prototype.position = function(position){
      const builder = new positionBuilder();
      this._position = builder
            .x(position.x)
            .y(position.y)
            .width(position.width)
            .height(position.height)
            .build();
      return this;
    };
    
    eventinfoBuilder.prototype.metaInfo = function(metaInfo){
      this._metaInfo = metaInfo;
      return this;
    };
  
    eventinfoBuilder.prototype.build = function() {
    var obj = new object(this._sentToServer,
                        this._objectName,
                        this._name,
                        this._urlPath,
                        this._tstmp,
                        this._position,
                        this._metaInfo);
    return obj;
  };
  
  export {eventinfoBuilder};