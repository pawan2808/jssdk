var api = function(){};

api.prototype.getRequest = function(url) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        return this.responseText;
      }
    };
    xhttp.open("GET", url, true);
    xhttp.send();
  };

  api.prototype.postRequest = function(url,payload) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        return this.responseText;
      }
    };
    xhttp.open("POST", url, true);
    xhttp.send(payload);
  };

  export {api};