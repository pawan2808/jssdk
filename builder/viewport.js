var object = function(timeStamp,width,height){
    this.timeStamp = timeStamp;
    this.width = width;
    this.height = height;
  };
  
  var viewportBuilder = function(){};
    
    viewportBuilder.prototype.timeStamp = function(timeStamp){
      this._timeStamp = timeStamp;
      return this;
    };
    
    viewportBuilder.prototype.width = function(width){
        this._width = width;
        return this;
    };
    
    viewportBuilder.prototype.height = function(height){
      this._height = height;
      return this;
    };
  
    viewportBuilder.prototype.build = function() {
    var obj = new object(this._timeStamp,
                        this._width,
                        this._height);
    return obj;
  };
  
  export {viewportBuilder};