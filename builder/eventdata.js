import {eventinfoBuilder} from './eventinfo.js';
import {deveventinfoBuilder} from './deveventinfo.js';
var object = function(eventsInfo,navigationPath,stayTimeBeforeNav,devCodifiedEventsInfo,sentToServer){
    this.eventsInfo = eventsInfo;
    this.navigationPath = navigationPath;
    this.stayTimeBeforeNav = stayTimeBeforeNav;
    this.devCodifiedEventsInfo = devCodifiedEventsInfo;
    this.sentToServer = sentToServer;
  };
  
  var eventBuilder = function(){};
    
    eventBuilder.prototype.eventsInfo = function(eventsInfo){
      const builder = new eventinfoBuilder();
      this._eventsInfo = eventsInfo.map((eventsInfo) => {
        return builder
              .sentToServer(eventsInfo.sentToServer)
              .objectName(eventsInfo.objectName)
              .name(eventsInfo.name)
              .urlPath(eventsInfo.urlPath)
              .tstmp(eventsInfo.tstmp)
              .position(eventsInfo.position)
              .metaInfo(eventsInfo.metaInfo)
              .build();      
        });
      return this;
    };
    
    eventBuilder.prototype.navigationPath = function(navigationPath){
        this._navigationPath = navigationPath;
        return this;
    };
    
    eventBuilder.prototype.stayTimeBeforeNav = function(stayTimeBeforeNav){
      this._stayTimeBeforeNav = stayTimeBeforeNav;
      return this;
    };
    
    eventBuilder.prototype.devCodifiedEventsInfo = function(devCodifiedEventsInfo){
      const builder = new deveventinfoBuilder();
      this._devCodifiedEventsInfo = devCodifiedEventsInfo.map((devCodifiedEventsInfo) => {
        return builder
              .sentToServer(devCodifiedEventsInfo.sentToServer)
              .objectName(devCodifiedEventsInfo.objectName)
              .name(devCodifiedEventsInfo.name)
              .urlPath(devCodifiedEventsInfo.urlPath)
              .tstmp(devCodifiedEventsInfo.tstmp)
              .duration(devCodifiedEventsInfo.duration)
              .metaInfo(devCodifiedEventsInfo.metaInfo)
              .build();      
        });
      return this;
    };
    
    eventBuilder.prototype.sentToServer = function(sentToServer){
      this._sentToServer = sentToServer;
      return this;
    };
  
    eventBuilder.prototype.build = function() {
    var obj = new object(this._eventsInfo,
                        this._navigationPath,
                        this._stayTimeBeforeNav,
                        this._devCodifiedEventsInfo,
                        this._sentToServer);
    return obj;
  };
  
  export {eventBuilder};