var object = function(x,y,width,height){
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  };
  
  var positionBuilder = function(){};
  
     positionBuilder.prototype.x = function(x){
         this._x = x;
         return this;
     };
    
    positionBuilder.prototype.y = function(y){
      this._y = y;
      return this;
    };
    
    positionBuilder.prototype.width = function(width){
      this._width = width;
      return this;
    };
    
    positionBuilder.prototype.height = function(height){
      this._height = height;
      return this;
    };
  
    positionBuilder.prototype.build = function() {
    var obj = new object(this._x,
                        this._y,
                        this._width,
                        this._height);
    return obj;
  };
  
  export {positionBuilder};